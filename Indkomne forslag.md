# Indkomne forslag

- Skilte ved hver kasse med grøntsager.
  Det gør det nemmere for medlemmer at finde grøntsager de ikke kender.
  Gerne genbrugelige kridttavler.

- Kartoffelposer.
  De er pr. 10. maj 2019 ikke i webshoppen.
  Jeg kender ikke årsagen, men jeg er sikker på det er midlertidigt.

- Poser med *de mest almindelige grøntsager (løg, kartofler osv.) som supplement til grøntposerne*.
  Som supplement til grøntposerne.
